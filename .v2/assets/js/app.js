var app = angular.module("CookieClicker", []);
app.controller("GameController", function($scope, $timeout) {
    $scope.cookies = 0;
    $scope.cookies_per_click = 1;
    $scope.cookies_per_second = 0.0;
    $scope.level_1 = 0;
    $scope.level_2 = 0;
    $scope.level_3 = 0;
    $scope.level_4 = 0;
    $scope.level_5 = 0;
    $scope.error = "";

    $scope.purchase_error_cookies = function(){
        $scope.error = "Not enough cookies";
        $timeout(function() {
            $scope.error = "";
        }, 2000);
    };

    $scope.purchase_succes = function(){
        $scope.error = "Purchase successful";
        $timeout(function() {
            $scope.error = "";
        }, 2000);
    };

    $scope.purchase_error_max_level = function(){
        $scope.error = "Max level reached";
        $timeout(function() {
            $scope.error = "";
        }, 2000);
    };

    $scope.getPriceLevel1 = function(){
        if ($scope.level_1 <= 5){
            return $scope.level_1 * 75 + 75;
        } else {
            return (5 + $scope.level_1) * 100;
        }
    };

    $scope.purchase_1 = function(){
        let current_cookies = $scope.cookies;
        let price = $scope.getPriceLevel1();
        if (current_cookies < price){
            $scope.purchase_error_cookies();
        } else {
            if ($scope.level_1 == 250){
                $scope.purchase_error_max_level();
            } else {
                $scope.cookies -= price;
                $scope.level_1 += 1;
                $scope.cookies_per_second += 1;
                $scope.purchase_succes();
            }
        }
    };

    $scope.getPriceLevel2 = function(){
        return (5 + $scope.level_2) * 250;
    };

    $scope.purchase_2 = function(){
        let current_cookies = $scope.cookies;
        let price = $scope.getPriceLevel2();
        if (current_cookies < price){
            $scope.purchase_error_cookies();
        } else {
            if ($scope.level_2 == 250){
                $scope.purchase_error_max_level();
            } else {
                $scope.cookies -= price;
                $scope.level_2 += 1;
                $scope.cookies_per_second += 5;
                $scope.purchase_succes();
            }
        }
    };

    $scope.getPriceLevel3 = function(){
        return (5 + $scope.level_3) * 500;
    };

    $scope.purchase_3 = function(){
        let current_cookies = $scope.cookies;
        let price = $scope.getPriceLevel3();
        if (current_cookies < price){
            $scope.purchase_error_cookies();
        } else {
            if ($scope.level_3 == 250){
                $scope.purchase_error_max_level();
            } else {
                $scope.cookies -= price;
                $scope.level_3 += 1;
                $scope.cookies_per_second += 10;
                $scope.purchase_succes();
            }
        }
    };

    $scope.getPriceLevel4 = function(){
        return (5 + $scope.level_4) * 1000;
    };

    $scope.purchase_4 = function(){
        let current_cookies = $scope.cookies;
        let price = $scope.getPriceLevel4();
        if (current_cookies < price){
            $scope.purchase_error_cookies();
        } else {
            if ($scope.level_4 == 250){
                $scope.purchase_error_max_level();
            } else {
                $scope.cookies -= price;
                $scope.level_4 += 1;
                $scope.cookies_per_second += 25;
                $scope.purchase_succes();
            }
        }
    };

    $scope.getPriceLevel5 = function(){
        return (5 + $scope.level_5) * 2500;
    };

    $scope.purchase_5 = function(){
        let current_cookies = $scope.cookies;
        let price = $scope.getPriceLevel5();
        if (current_cookies < price){
            $scope.purchase_error_cookies();
        } else {
            if ($scope.level_5 == 250){
                $scope.purchase_error_max_level();
            } else {
                $scope.cookies -= price;
                $scope.level_5 += 1;
                $scope.cookies_per_second += 50;
                $scope.purchase_succes();
            }
        }
    };

    $scope.add_cookies = function(){
        $scope.cookies = $scope.cookies + $scope.cookies_per_click;
    };

    $scope.start = function addSecondCookies(){
        $timeout(function() {
            $scope.cookies = $scope.cookies + $scope.cookies_per_second;
            addSecondCookies();
        }, 1000);
    };
});