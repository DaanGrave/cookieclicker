var app = angular.module("CookieClicker", []);
app.controller("GameController", function($scope, $timeout) {
    $scope.cookies = 0;
    $scope.cookies_per_click = 1;
    $scope.cookies_per_second = 0;
    $scope.add_cookies = function(){
        $scope.cookies = $scope.cookies + $scope.cookies_per_click;
    };
    $scope.getNiceCookiesPerClick = function(){
        return $scope.cookies_per_click + " (" + ($scope.cookies_per_click * 50) + ")";
    };
    $scope.getNiceCookiesPerSecond = function(){
        return $scope.cookies_per_second + " (" + ($scope.cookies_per_second * 100 + 100) + ")";
    };
    $scope.multiply_cookies_per_click = function(){
        let required_cookies = $scope.cookies_per_click * 34;
        if ($scope.cookies >= required_cookies){
            $scope.cookies = $scope.cookies - required_cookies;
            $scope.cookies_per_click = $scope.cookies_per_click * 2;
        }
    };
    $scope.add_cookies_per_sec = function(){
        let required_cookies = $scope.cookies_per_second * 100 + 100;
        if ($scope.cookies >= required_cookies){
            $scope.cookies = $scope.cookies - required_cookies;
            $scope.cookies_per_second += 1;
        }
    };
    $scope.start = function addSecondCookies(){
        $timeout(function() {
            $scope.cookies = $scope.cookies + $scope.cookies_per_second;
            addSecondCookies();
        }, 1000);
    };
});
